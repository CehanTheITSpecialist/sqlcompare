object Form1: TForm1
  Left = 190
  Top = 219
  ActiveControl = edt_searchvalue
  Caption = 'SQL Compare'
  ClientHeight = 654
  ClientWidth = 1038
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 15
  object StatusBar1: TStatusBar
    Left = 0
    Top = 635
    Width = 1038
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1038
    Height = 635
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 1
    object ResultGrid: TStringGrid
      Left = 1
      Top = 99
      Width = 1036
      Height = 535
      Align = alClient
      ColCount = 4
      DefaultRowHeight = 17
      DefaultDrawing = False
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      GridLineWidth = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goRowSelect]
      ParentFont = False
      TabOrder = 0
      OnDrawCell = ResultGridDrawCell
    end
    object Panel2: TPanel
      Left = 1
      Top = 76
      Width = 1036
      Height = 23
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object lblFile1: TLabel
        Left = 0
        Top = 5
        Width = 36
        Height = 15
        Caption = ' File1: '
      end
      object lblFile2: TLabel
        Left = 381
        Top = 5
        Width = 36
        Height = 15
        Caption = ' File2: '
      end
    end
    object pnl_Top: TPanel
      Left = 1
      Top = 1
      Width = 1036
      Height = 75
      Align = alTop
      TabOrder = 2
      object edt_searchvalue: TEdit
        Left = 8
        Top = 8
        Width = 702
        Height = 23
        Hint = 'Search'
        TabOrder = 0
        OnKeyPress = edt_searchvalueKeyPress
      end
      object edt_Connection1: TComboBox
        Tag = 99
        Left = 8
        Top = 46
        Width = 329
        Height = 23
        TabOrder = 1
        OnChange = edt_Connection1Change
      end
      object edt_Connection2: TComboBox
        Tag = 99
        Left = 381
        Top = 46
        Width = 329
        Height = 23
        TabOrder = 2
        OnChange = edt_Connection2Change
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 112
    Top = 137
    object File1: TMenuItem
      Caption = '&File'
      object Open11: TMenuItem
        Caption = 'Open &1 ...'
        ShortCut = 16433
        OnClick = Open11Click
      end
      object Open21: TMenuItem
        Caption = 'Open &2 ...'
        ShortCut = 16434
        OnClick = Open21Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object mnuCompare: TMenuItem
        Caption = '&Compare'
        Enabled = False
        ShortCut = 120
        OnClick = mnuCompareClick
      end
      object mnuCancel: TMenuItem
        Caption = 'C&ancel'
        Enabled = False
        ShortCut = 27
        OnClick = mnuCancelClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object mnuView: TMenuItem
      Caption = '&View'
      Enabled = False
      object PreviousChanges1: TMenuItem
        Caption = '&Previous Changes'
        ShortCut = 16464
        OnClick = PreviousChanges1Click
      end
      object NextChanges1: TMenuItem
        Caption = '&Next Changes'
        ShortCut = 16462
        OnClick = NextChanges1Click
      end
    end
    object Options1: TMenuItem
      Caption = '&Options'
      object mnuIgnoreCase: TMenuItem
        Caption = 'Ignore &Case'
        OnClick = mnuIgnoreCaseClick
      end
      object mnuIgnoreWhiteSpace: TMenuItem
        Caption = 'Ignore &White Space'
        OnClick = mnuIgnoreWhiteSpaceClick
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 183
    Top = 119
  end
end
