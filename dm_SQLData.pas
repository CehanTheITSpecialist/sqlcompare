unit dm_SQLData;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB;

type
  Tdtm_SQLData = class(TDataModule)
    DBConnection: TADOConnection;
    qry_ObjectName: TADOQuery;
  private

  public
    class function GetObjectScript(const AConnectionString, AnObjectName: String): String;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

class function Tdtm_SQLData.GetObjectScript(const AConnectionString, AnObjectName: String): String;
var
  Data : Tdtm_SQLData;
begin
  result := '';

  Data := Tdtm_SQLData.Create(Nil);
  try
    with Data, qry_ObjectName do begin
      DBConnection.ConnectionString := AConnectionString;
      try
        DBConnection.Connected := True;
      except
        on e:Exception do begin
          Exception.Create(Format('Could not connect to the database engine: %s %s', [e.StackTrace, AConnectionString]));


          Exit;
        end;
      end;


      SQL.Text := Format('SELECT definition FROM sys.sql_modules WHERE object_id = (OBJECT_ID(''%s''))', [AnObjectName]);

      Close;
      Open;

      if not IsEmpty then
        result := qry_ObjectName.Fields[0].Value;


    end;
  finally
    Data.Free;
  end;
end;

end.
